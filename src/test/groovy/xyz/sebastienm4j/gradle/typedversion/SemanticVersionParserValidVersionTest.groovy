package xyz.sebastienm4j.gradle.typedversion

import org.junit.Test
import xyz.sebastienm4j.gradle.typedversion.semantic.SemanticVersion
import xyz.sebastienm4j.gradle.typedversion.semantic.SemanticVersionParser

import static org.assertj.core.api.Assertions.assertThat

class SemanticVersionParserValidVersionTest
{
    @Test
    void "parse a valid semantic version without pre release"() {
        String givenVersion = '2.0.18'

        SemanticVersion version = SemanticVersionParser.parse(givenVersion)

        assertThat(version).isNotNull()
        assertThat(version.getMajor()).isEqualTo(2)
        assertThat(version.getMinor()).isEqualTo(0)
        assertThat(version.getPatch()).isEqualTo(18)
        assertThat(version.getPreRelease()).isNull()
    }

    @Test
    void "parse a valid semantic version with pre release"() {
        String givenVersion = '2.0.18-1.b38.124123'

        SemanticVersion version = SemanticVersionParser.parse(givenVersion)

        assertThat(version).isNotNull()
        assertThat(version.getMajor()).isEqualTo(2)
        assertThat(version.getMinor()).isEqualTo(0)
        assertThat(version.getPatch()).isEqualTo(18)
        assertThat(version.getPreRelease()).isEqualTo("1.b38.124123")
    }

    @Test
    void "parse a valid semantic version without pre release but with ignored build metadata"() {
        String givenVersion = '2.0.18+10.azerty.021'

        SemanticVersion version = SemanticVersionParser.parse(givenVersion)

        assertThat(version).isNotNull()
        assertThat(version.getMajor()).isEqualTo(2)
        assertThat(version.getMinor()).isEqualTo(0)
        assertThat(version.getPatch()).isEqualTo(18)
        assertThat(version.getPreRelease()).isNull()
    }

    @Test
    void "parse a valid semantic version with pre release and ignored build metadata"() {
        String givenVersion = '2.0.18-10.b38.124123.AMD64+007.FR.2'

        SemanticVersion version = SemanticVersionParser.parse(givenVersion)

        assertThat(version).isNotNull()
        assertThat(version.getMajor()).isEqualTo(2)
        assertThat(version.getMinor()).isEqualTo(0)
        assertThat(version.getPatch()).isEqualTo(18)
        assertThat(version.getPreRelease()).isEqualTo("10.b38.124123.AMD64")
    }
}
