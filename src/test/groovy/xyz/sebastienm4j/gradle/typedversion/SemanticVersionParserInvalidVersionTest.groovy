package xyz.sebastienm4j.gradle.typedversion

import org.junit.Test
import org.junit.runner.RunWith
import org.junit.runners.Parameterized
import xyz.sebastienm4j.gradle.typedversion.semantic.SemanticVersionParser

import java.text.ParseException

@RunWith(Parameterized.class)
class SemanticVersionParserInvalidVersionTest
{
    @Parameterized.Parameters
    static Iterable<? extends Object> data() {
        return Arrays.asList(
                null,
                "",
                "02.0.18",
                "2.00.18",
                "2.0.018",
                "a.0.18",
                "2.b.18",
                "2.0.c",
                ".0.18",
                "2..18",
                "2.0..",
                "-1.2.3",
                "2.0.18-01.2.3",
                "2.0.18-1.02.3",
                "2.0.18-1.2.03",
                "2.0.18-.b.c",
                "2.0.18-a..c",
                "2.0.18-a.b.",
                "+aa.bb.cc",
                "2.0.18-+1.2.3",
                "2.0.18+.bb.cc",
                "2.0.18+aa..cc",
                "2.0.18+aa.bb.")
    }

    SemanticVersionParserInvalidVersionTest(String givenVersion)
    {
        this.givenVersion = givenVersion
    }

    private String givenVersion


    @Test(expected = ParseException.class)
    void "ParseException with invalid semantic version"() {
        SemanticVersionParser.parse(givenVersion)
    }
}
