package xyz.sebastienm4j.gradle.typedversion.semantic

/**
 * Represent a semantic version (see https://semver.org/).
 *
 * <p>
 *     Note that build metadata is not considered because
 *     it must be added dynamically at build.
 * </p>
 */
class SemanticVersion
{
    int major
    int minor
    int patch
    String preRelease

    @Override
    String toString()
    {
        String version = major+"."+minor+"."+patch
        if(preRelease != null) {
            version += "-"+preRelease
        }
        return version
    }
}
