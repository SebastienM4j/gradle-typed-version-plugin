package xyz.sebastienm4j.gradle.typedversion.semantic

import org.slf4j.Logger
import org.slf4j.LoggerFactory

import java.text.ParseException

/**
 * Parser for a {@link SemanticVersion}.
 */
class SemanticVersionParser
{
    static final Logger logger = LoggerFactory.getLogger(SemanticVersionParser.class)


    /**
     * Check and parse the given version as semantic version.
     *
     * @param version The semantic version
     * @return A typed semantic version
     */
    static SemanticVersion parse(String version)
    {
        def regex = (version =~ /^(?<major>0|[1-9]\d*)\.(?<minor>0|[1-9]\d*)\.(?<patch>0|[1-9]\d*)(?<preRelease>-(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*)(\.(0|[1-9]\d*|\d*[a-zA-Z-][0-9a-zA-Z-]*))*)?(?<buildMetadata>\+[0-9a-zA-Z-]+(\.[0-9a-zA-Z-]+)*)?$/)
        if(!regex.matches()) {
            throw new ParseException("Version ["+version+"] is not a semantic version", 0)
        }

        SemanticVersion semanticVersion = new SemanticVersion()
        semanticVersion.setMajor(Integer.parseInt(regex.group("major")))
        semanticVersion.setMinor(Integer.parseInt(regex.group("minor")))
        semanticVersion.setPatch(Integer.parseInt(regex.group("patch")))

        String preRelease = regex.group("preRelease")
        if(preRelease != null) {
            semanticVersion.setPreRelease(preRelease.replaceFirst("-", ""))
        }

        String buildMetadata = regex.group("buildMetadata")
        if(buildMetadata != null) {
            logger.warn("Build metadata [{}] of the semantic version [{}] is ignored", buildMetadata, version)
        }

        return semanticVersion
    }
}
