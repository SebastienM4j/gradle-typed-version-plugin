package xyz.sebastienm4j.gradle.typedversion

import org.gradle.testkit.runner.GradleRunner
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

import static org.gradle.testkit.runner.TaskOutcome.SUCCESS

class TypedVersionPluginSpec extends Specification
{
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    File buildFile


    def setup()
    {
        buildFile = testProjectDir.newFile('build.gradle')
        buildFile << """
            plugins {
                id 'xyz.sebastienm4j.gradle.typed-version'
            }
        """
    }


    def "good version property with raw version"() {
        buildFile << """
            version = raw(' 1.0#5a ')
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('properties')
                .withPluginClasspath()
                .build()

        then:
        result.output.contains("version:  1.0#5a ")
        result.task(":properties").outcome == SUCCESS
    }

    def "good version property with semantic version without pre release"() {
        buildFile << """
            version = semantic('2.0.18')
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('properties')
                .withPluginClasspath()
                .build()

        then:
        result.output.contains("version: 2.0.18")
        result.task(":properties").outcome == SUCCESS
    }

    def "good version property with semantic version with pre release"() {
        buildFile << """
            version = semantic('2.0.18-alpha.12.35.B')
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('properties')
                .withPluginClasspath()
                .build()

        then:
        result.output.contains("version: 2.0.18-alpha.12.35.B")
        result.task(":properties").outcome == SUCCESS
    }

    def "semantic version properties with semantic version with pre release"() {
        buildFile << """
            version = semantic('2.0.18-alpha.12.35.B')
            
            println "major: \${version.major}"
            println "minor: \${version.minor}"
            println "patch: \${version.patch}"
            println "preRelease: \${version.preRelease}"
        """

        when:
        def result = GradleRunner.create()
                .withProjectDir(testProjectDir.root)
                .withArguments('properties')
                .withPluginClasspath()
                .build()

        then:
        result.output.contains("version: 2.0.18-alpha.12.35.B")
        result.output.contains("major: 2")
        result.output.contains("minor: 0")
        result.output.contains("patch: 18")
        result.output.contains("preRelease: alpha.12.35.B")
        result.task(":properties").outcome == SUCCESS
    }
}
