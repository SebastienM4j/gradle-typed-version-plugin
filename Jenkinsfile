#!groovy

def projectProperties = [
        [$class: 'BuildDiscarderProperty',strategy: [$class: 'LogRotator', numToKeepStr: '15']],
]
properties(projectProperties)


node {

    stage('Build') {
        checkout scm
        sh './gradlew clean classes'
    }

    stage('Test') {
        try {
            sh './gradlew check'
            sh './gradlew coverage'

        } finally {
            step $class: 'JUnitResultArchiver', testResults: '**/build/test-results/**/TEST-*.xml'
        }
    }

    stage('Quality') {
        withSonarQubeEnv('sonarqube') {
            sh './gradlew --info sonarqube'
        }
    }
}

stage('Quality Gate') {
    timeout(time: 1, unit: 'HOURS') {
        def qg = waitForQualityGate()
        if (qg.status != 'OK') {
            error "Pipeline aborted due to quality gate failure: ${qg.status}"
        }
    }
}

node {
    stage('Artifacts') {
        sh './gradlew assemble'
        sh "./gradlew publishMavenJavaPublicationToMavenRepository -PmavenReleasesURL=${env.MAVEN_RELEASES_URL} -PmavenUsername=${env.MAVEN_USERNAME} -PmavenPassword=${env.MAVEN_PASSWORD}"
    }
}
