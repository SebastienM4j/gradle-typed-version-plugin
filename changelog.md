Gradle Typed Version Plugin Change Log
======================================

0.1.0 / _2018-06-26_
--------------------

* Support of [semantic versioning](https://semver.org/)
